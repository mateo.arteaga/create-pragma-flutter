import 'dart:io';
import 'package:io/io.dart';
import 'package:create_pragma_flutter/const.dart';
import 'package:path/path.dart' as path;

Future<void> scaffoldProject(String projectName, String projectDir) async {
  final libDir = path.join(pkgRoot, "template/base");
  try {
    final result = await Process.run('flutter', ['create', projectName]);

    await copyPath(libDir, projectDir);

    stdout.writeln(result.stdout);
  } catch (e) {
    stderr.writeln("something whent wrong : $e");
  }
}
