import 'package:create_pragma_flutter/const.dart';
import 'package:create_pragma_flutter/helpers/scaffold_project.dart';
import 'package:path/path.dart' as path;

class CreateProject {
  String projectName;
  late String projectDir;

  CreateProject({this.projectName = appName}) {
    projectDir = "${path.current}/$projectName";
  }

  Future<void> createProject() async {
    await scaffoldProject(projectName, projectDir);
  }
}
