List<String> parseNameAndPath(String input) {
  final paths = input.split("/");

  String appName = paths[paths.length - 1];

  // If the first part is a @, it's a scoped package
  final indexOfDelimiter = paths.indexWhere((p) => p.startsWith("@"));

  if (paths.indexWhere((p) => p.startsWith("@")) != -1) {
    appName = paths.sublist(indexOfDelimiter).join("/");
  }

  final path = paths.where((p) => p.startsWith("@")).join("/");

  return [appName, path];
}
