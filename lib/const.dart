import 'dart:io';
import 'package:path/path.dart' as path;

// gets the root of the dir where the script is located
final _pkgPath = path.dirname(Platform.script.path).split("/");
final pkgRoot = _pkgPath.sublist(0, _pkgPath.length - 1).join("/");

const appName = "sample_flutter_app";
