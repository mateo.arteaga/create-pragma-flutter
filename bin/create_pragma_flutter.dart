import 'package:create_pragma_flutter/const.dart';
import 'package:create_pragma_flutter/helpers/create_project.dart';
import 'package:create_pragma_flutter/helpers/scaffold_project.dart';
import 'package:create_pragma_flutter/utils/parse_name_and_path.dart';

Future<void> main(List<String> arguments) async {
  final res = parseNameAndPath(appName);
  final scopedAppName = res[0];
  final path = res[1];

  await CreateProject().createProject();
}
